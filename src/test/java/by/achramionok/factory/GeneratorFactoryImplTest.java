package by.achramionok.factory;

import by.achramionok.factory.GeneratorFactory;
import by.achramionok.factory.GeneratorFactoryImpl;
import by.achramionok.generator.DateGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;

public class GeneratorFactoryImplTest {
    @Rule
    public  ExpectedException exception = ExpectedException.none();

    @Test
    public void testGetGenerator() throws Exception {
        GeneratorFactory factory = new GeneratorFactoryImpl();
        assertNotNull(factory.getGenerator("date:21/10/2014"));
        assertNotNull(factory.getGenerator("number:214"));
        assertNotNull(factory.getGenerator("string:20"));
    }

    @Test
    public void throwsIllegalStateExceptionIfDateParameterIsWrong(){
        GeneratorFactory factory = new GeneratorFactoryImpl();
        exception.expect(IllegalStateException.class);
        factory.getGenerator("date:21-10/200q");
    }
    @Test
    public void throwsIllegalStateExceptionIfNumberParameterIsWrong(){
        GeneratorFactory factory = new GeneratorFactoryImpl();
        exception.expect(IllegalStateException.class);
        factory.getGenerator("number:q21");
        factory.getGenerator("number:21q");
        factory.getGenerator("number:");
    }
    @Test
    public void throwsIllegalStateExceptionIfStringParameterIsWrong(){
        GeneratorFactory factory = new GeneratorFactoryImpl();
        exception.expect(IllegalStateException.class);
        factory.getGenerator("string:sqf");
    }

}