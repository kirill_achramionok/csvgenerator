package by.achramionok.generator;

import by.achramionok.generator.DateGenerator;
import by.achramionok.generator.Generator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class DateGeneratorTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void returnTrueIfParameterValid() throws Exception {
        assertTrue(DateGenerator.isValidParameter("date:21/01/2014"));
    }

    @Test
    public void returnFalseIfParameterIsWrong(){
        assertFalse(DateGenerator.isValidParameter("date:21-01-14"));
    }

    @Test
    public void testGenerateValue() throws Exception {
        Generator generator = new DateGenerator("date:21/10/2014");
        assertNotNull(generator.generateValue());
        assertTrue(generator.generateValue().toString().matches("^([0-9]{2})/([0-9]{2})/([0-9]+)$"));
    }

    @Test
    public void throwIllegalStateExceptionIfParameterIsWrong(){
        exception.expect(IllegalStateException.class);
        Generator generator = new DateGenerator("date:21-10-2014");
        generator.generateValue();

    }

}