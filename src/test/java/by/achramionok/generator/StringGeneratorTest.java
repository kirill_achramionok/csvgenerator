package by.achramionok.generator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class StringGeneratorTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void returnTrueIfParameterIsValid() throws Exception {
        assertTrue(StringGenerator.isValidParameter("string:10"));
        assertTrue(StringGenerator.isValidParameter("String:10"));
        assertTrue(StringGenerator.isValidParameter("stRing:10"));
    }

    @Test
    public void returnFalseIfParameterIsWrong() throws Exception {
        assertFalse(StringGenerator.isValidParameter("strin:10"));
        assertFalse(StringGenerator.isValidParameter("String:"));
        assertFalse(StringGenerator.isValidParameter(":"));
        assertFalse(StringGenerator.isValidParameter(":10"));
        assertFalse(StringGenerator.isValidParameter("stRing:10q"));
    }

    @Test
    public void TestGenerateValue() throws Exception {
        StringGenerator generator = new StringGenerator("string:20");
        String value = generator.generateValue();
        assertEquals(20, value.length());
        assertTrue(value.matches("^[a-zA-Z]+$"));

    }

    @Test
    public void throwIllegalStateExceptionIfParameterIsWrong() throws Exception {
        exception.expect(IllegalStateException.class);
        StringGenerator generator = new StringGenerator("string:qw");
        generator.generateValue();
    }

}