package by.achramionok.generator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class NumberGeneratorTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void returnTrueIfParameterIsValid() throws Exception {
        assertTrue(NumberGenerator.isValidParameter("number:30"));
        assertTrue(NumberGenerator.isValidParameter("Number:30"));
    }

    @Test
    public void returnFalseIfParameterIsWrong() throws Exception {
        assertFalse(NumberGenerator.isValidParameter("umber:20"));
        assertFalse(NumberGenerator.isValidParameter(":20"));
        assertFalse(NumberGenerator.isValidParameter(":"));
        assertFalse(NumberGenerator.isValidParameter("number:"));
        assertFalse(NumberGenerator.isValidParameter("number:20q"));
        assertFalse(NumberGenerator.isValidParameter("number:2 0"));
    }

    @Test
    public void testGenerateValue() throws Exception {
        NumberGenerator generator = new NumberGenerator("number:10");
        assertNotNull(generator.generateValue());
        assertEquals(10, generator.generateValue().length());
        assertTrue(generator.generateValue().matches("[0-9]+"));
    }

    @Test
    public void throwIllegalStateExceptionIfParameterValueIsWrong() {
        exception.expect(IllegalStateException.class);
        NumberGenerator generator = new NumberGenerator("number:qwe");
        generator.generateValue();
    }

}