package by.achramionok;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class CSVWriterTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void writeToFile() throws Exception {
        CSVWriter writer = new CSVWriter();
        writer.writeToFile("D:\\file.txt", 10, "string:10");
        assertTrue(new File("D:\\file.txt").exists());
    }

    @Test
    public void returnTrueIfFileIsCreated() throws Exception {
        CSVWriter writer = new CSVWriter();
        assertTrue(writer.createFile("D:\\file.txt").exists());
    }

    @Test
    public void fileIsNotCreated() throws Exception {
        CSVWriter writer = new CSVWriter();
        assertFalse(writer.createFile("D\\qsf").exists());

    }

}