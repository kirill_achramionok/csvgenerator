package by.achramionok.generator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class StringGenerator implements Generator {
    private static Logger log = LoggerFactory.getLogger(StringGenerator.class.getName());
    private String parameter;

    public StringGenerator(String parameter) {
        this.parameter = parameter;
        log.info("Object created");
    }

    public static boolean isValidParameter(String parameter) {
        return parameter.matches("^[Ss][Tt][Rr][Ii][Nn][Gg]:[0-9]+$");
    }

    private String getString() {
        StringBuilder builder = new StringBuilder();
        String symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int length = getStringLength();
        for (int i = 0; i < length; i++) {
            builder.append(symbols.charAt(new Random().nextInt(symbols.length())));
        }
        return builder.toString();
    }

    @Override
    public String generateValue() {
        return getString();
    }

    private Integer getStringLength() {
        if(isValidParameter(parameter)){
            return Integer.parseInt(parameter.replaceAll("[^\\d]", ""));
        }else{
            throw new IllegalStateException("Illegal string format");
        }

    }

}
