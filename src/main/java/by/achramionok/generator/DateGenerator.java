package by.achramionok.generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class DateGenerator implements Generator {
    private static Logger log = LoggerFactory.getLogger(DateGenerator.class.getName());
    private SimpleDateFormat dateFormat;

    public DateGenerator(String parameter) {
        setDateFormat(parameter);
        log.info("Object created");
    }

    public static boolean isValidParameter(String parameter) {
        return parameter.matches("^[Dd][Aa][Tt][Ee]:([0-9]{2})/([0-9]{2})/([0-9]{4})$");
    }

    @Override
    public Object generateValue() {
        return dateFormat.format(
                new Date(Math.abs(System.currentTimeMillis() - new Random().nextLong()))
        );
    }

    private void setDateFormat(String parameter) {
        if (isValidParameter(parameter)) {
            dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        } else {
            throw new IllegalStateException("Illegal date format");
        }
    }

}
