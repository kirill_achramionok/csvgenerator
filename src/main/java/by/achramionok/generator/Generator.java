package by.achramionok.generator;

public interface Generator {
    Object generateValue();
}
