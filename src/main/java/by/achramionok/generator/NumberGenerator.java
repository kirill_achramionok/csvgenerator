package by.achramionok.generator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class NumberGenerator implements Generator {
    private static Logger log = LoggerFactory.getLogger(NumberGenerator.class.getName());
    private String parameter;

    public NumberGenerator(String parameter) {
        this.parameter = parameter;
        log.info("Object created");
    }

    public static boolean isValidParameter(String parameter) {
        return parameter.matches("^[Nn][Uu][Mm][Bb][Ee][Rr]:[0-9]+$");
    }

    @Override
    public String generateValue() {
        int length = getNumberLength();
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            builder.append(random.nextInt(10));
        }
        return builder.toString();
    }

    private int getNumberLength() {
        if(isValidParameter(parameter)){
        return Integer.parseInt(parameter.replaceAll("[^\\d]", ""));
        }else{
            throw new IllegalStateException ("Wrong parameter");
        }
    }


}
