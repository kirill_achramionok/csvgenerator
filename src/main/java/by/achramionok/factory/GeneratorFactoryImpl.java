package by.achramionok.factory;

import by.achramionok.generator.DateGenerator;
import by.achramionok.generator.Generator;
import by.achramionok.generator.NumberGenerator;
import by.achramionok.generator.StringGenerator;


public class GeneratorFactoryImpl implements GeneratorFactory {
    public Generator getGenerator(String parameter) {
        if (DateGenerator.isValidParameter(parameter)) {
            return new DateGenerator(parameter);
        } else if (NumberGenerator.isValidParameter(parameter)) {
            return new NumberGenerator(parameter);
        } else if (StringGenerator.isValidParameter(parameter)) {
            return new StringGenerator(parameter);
        } else {
            throw new IllegalStateException();
        }
    }

}
