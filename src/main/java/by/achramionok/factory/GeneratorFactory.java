package by.achramionok.factory;

import by.achramionok.generator.Generator;

public interface GeneratorFactory {
    Generator getGenerator(String pair);
}
