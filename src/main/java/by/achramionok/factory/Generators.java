package by.achramionok.factory;

import by.achramionok.generator.DateGenerator;
import by.achramionok.generator.Generator;
import by.achramionok.generator.NumberGenerator;
import by.achramionok.generator.StringGenerator;

/**
 * @author Pavel.B
 * Cteated 20-Oct-17.
 */
public enum Generators {

    STR(StringGenerator::new, StringGenerator::isValidParameter),
    NUM(NumberGenerator::new, NumberGenerator::isValidParameter),
    DATE(DateGenerator::new, DateGenerator::isValidParameter);

    private final GeneratorCreator creator;
    private final Validator validator;

    Generators(GeneratorCreator creator, Validator validator) {
        this.creator = creator;
        this.validator = validator;
    }

    public boolean valid(String param) {
        return this.validator.apply(param);
    }

    public Generator create(String param) {
        return this.creator.apply(param);
    }

    @FunctionalInterface
    public interface GeneratorCreator {
        Generator apply(String parameter);
    }

    @FunctionalInterface
    public interface Validator {
        boolean apply(String parameter);
    }
}
