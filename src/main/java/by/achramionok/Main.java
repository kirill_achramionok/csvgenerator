package by.achramionok;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static Logger log = LoggerFactory.getLogger(Main.class.getName());

    public static void main(String[] args) {
        try {
            CSVWriter writer = new CSVWriter();
            long start = System.currentTimeMillis();
//            writer.writeToFile(args[2], Integer.valueOf(args[1]), args[0]);
            writer.writeToFile("D:\\file.csv", 340, "string:20;date:24/10/2014;number:10");
            long finish = System.currentTimeMillis();
            System.out.println((finish - start) / 1000);
        } catch (Exception ex) {
            log.error("Exception: ", ex);
        }

    }
}
//34000000