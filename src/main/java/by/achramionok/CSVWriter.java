package by.achramionok;

import by.achramionok.factory.Generators;
import by.achramionok.generator.Generator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class CSVWriter {
    private static Logger log = LoggerFactory.getLogger(CSVWriter.class.getName());

    File createFile(String filePath) {
        File CSVFile = new File(filePath);
        try {
            if (!CSVFile.exists()) {
                if (CSVFile.createNewFile()) {
                    log.info("File created");
                }
            }
        } catch (IOException e) {
            log.error("Exception: ", e);
        }
        return CSVFile;
    }

    public void writeToFile(String filePath, long size, String parameters) throws IOException {
        List<Generator> generators = new ArrayList<>();
        for (String s : parameters.split(";")) {
            generators.add(Stream.of(Generators.values()).filter(generator -> generator.valid(s)).findAny().get().create(s));
        }
        FileWriter writer = null;
        try {
            writer = new FileWriter(createFile(filePath));
            for (long i = 0; i < size; i++) {
                for (Generator generator : generators) {
                    writer.append(generator.generateValue().toString()).append(',');
                }
                writer.append('\n');
            }
        } finally {
            if (writer != null)
                writer.close();
        }
    }

}
